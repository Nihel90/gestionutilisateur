package com.example.userauthentif;

import android.graphics.BitmapFactory;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.userauthentif.R;
import com.example.userauthentif.User;

import java.io.ByteArrayOutputStream;
import java.util.List;
import  java.util.*;
import  android.graphics.drawable.*;
import  android.graphics.*;
import android.widget.ImageView;
import android.widget.Toast;

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.UserViewHolder> {

    private List<User> listUsers;

    public UsersRecyclerAdapter(List<User> listUsers) {
        this.listUsers = listUsers;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflating recycler item view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user_recycler, parent, false);

        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        try{
            holder.textViewName.setText(listUsers.get(position).getName());
            holder.textViewEmail.setText(listUsers.get(position).getEmail());
            holder.textViewPassword.setText(listUsers.get(position).getPassword());
            holder.textViewPays.setText(listUsers.get(position).getCountry());
            holder.textViewPaysPhys.setText(listUsers.get(position).getCountryloc());
            holder.textViewAdresse.setText(listUsers.get(position).getAddress());

  /*          if(listUsers.get(position).getImage()!= null)
            {
                Bitmap bitmap = BitmapFactory.decodeByteArray(listUsers.get(position).getImage(), 0, listUsers.get(position).getImage().length);
                holder.avatarProfileUser.setImageBitmap (bitmap);
            }

          */
        }
        catch (Exception ex){
            Log.e("erreur : ",ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        Log.v(UsersRecyclerAdapter.class.getSimpleName(),""+listUsers.size());
        return listUsers.size();
    }


    /**
     * ViewHolder class
     */
    public class UserViewHolder extends RecyclerView.ViewHolder {


        public AppCompatTextView textViewName;
        public AppCompatTextView textViewEmail;
        public AppCompatTextView textViewPassword;
        public AppCompatTextView textViewPays;
        public AppCompatTextView textViewPaysPhys;
        public AppCompatTextView textViewAdresse;

        public ImageView avatarProfileUser;

        public UserViewHolder(View view) {

            super(view);
            try {
                textViewName = (AppCompatTextView) view.findViewById(R.id.textViewName);
                textViewEmail = (AppCompatTextView) view.findViewById(R.id.textViewEmail);
                textViewPassword = (AppCompatTextView) view.findViewById(R.id.textViewPassword);
                textViewPays = (AppCompatTextView) view.findViewById(R.id.textViewPays);
                textViewPaysPhys = (AppCompatTextView) view.findViewById(R.id.textViewPaysPhys);
                textViewAdresse = (AppCompatTextView) view.findViewById(R.id.textViewAdresse);



                avatarProfileUser=(ImageView) view.findViewById((R.id.avatarProfileUser));
                }
            catch (Exception ex){
                Log.e("erreur dd : ",ex.getMessage());
            }
        }
    }


}
