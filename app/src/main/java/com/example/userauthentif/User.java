package com.example.userauthentif;

import java.sql.Blob;

public class User {

    private int id;
    private String name;
    private String email;
    private String password;
    private byte[] image;
    private String country;
    private Double latitude;
    private Double longitude;
    private String address;
    private String countryloc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public void setCountry(String country) {
        this.country= country;
    }

    public String getCountry() { return country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String street) {
        this.address = street;
    }



    public String getCountryloc() {
        return countryloc;
    }

    public void setCountryloc(String countryloc) {
        this.countryloc = countryloc;
    }


}
