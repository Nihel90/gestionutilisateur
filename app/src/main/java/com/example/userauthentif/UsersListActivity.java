package com.example.userauthentif;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.userauthentif.R;
import com.example.userauthentif.UsersRecyclerAdapter;
import com.example.userauthentif.User;
import com.example.userauthentif.DatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class UsersListActivity extends AppCompatActivity {

    private AppCompatActivity activity = UsersListActivity.this;
    private AppCompatTextView textViewName;
    private RecyclerView recyclerViewUsers;
    private List<User> listUsers;
    private UsersRecyclerAdapter usersRecyclerAdapter;
    private DatabaseHelper databaseHelper;
    public String emailUser="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);
        getSupportActionBar().setTitle("");
        initViews();
        try{
            //emailUser = getIntent().getStringExtra("emailUser");
            //textViewName.setText(emailUser);


            initObjects();
        }
        catch (Exception e)
        {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        //textViewName = (AppCompatTextView) findViewById(R.id.textViewName);
        recyclerViewUsers = (RecyclerView) findViewById(R.id.recyclerViewUsers);
    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        try{


            listUsers = new ArrayList<>();
            usersRecyclerAdapter = new UsersRecyclerAdapter(listUsers);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerViewUsers.setLayoutManager(mLayoutManager);
            recyclerViewUsers.setItemAnimator(new DefaultItemAnimator());
            recyclerViewUsers.setHasFixedSize(true);
            recyclerViewUsers.setAdapter(usersRecyclerAdapter);
            databaseHelper = new DatabaseHelper(activity);



            getDataFromSQLite();
        }catch (Exception e)
        {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is to fetch all user records from SQLite
     */
    private void getDataFromSQLite() {
        try{
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    listUsers.clear();
                    listUsers.addAll(databaseHelper.getAllUser());

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    usersRecyclerAdapter.notifyDataSetChanged();
                }
            }.execute();
        }catch (Exception ex)
        {
            Log.e ("erreur",ex.getMessage());
            Toast.makeText(activity, ex.getMessage(), Toast.LENGTH_SHORT).show();

        }
        // AsyncTask is used that SQLite operation not blocks the UI Thread.

    }
}
