package com.example.userauthentif;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.userauthentif.R;
import com.example.userauthentif.UsersRecyclerAdapter;
import com.example.userauthentif.User;
import com.example.userauthentif.DatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class ProfileActivity extends AppCompatActivity {


    private DatabaseHelper databaseHelper;
    public String emailUser="";
    public TextView textViewNameProfile;
    public TextView textViewEmailProfile;
    public TextView textViewCountryProfile;
    public TextView textViewLatitudeProfile;
    public TextView textViewLongitudeProfile;
    public ImageView imageProfile;
    public TextView textViewAddressProfile;
    public TextView textViewCountrylocProfile;
   // private User user;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setTitle("");
        initViews();
        try{
            emailUser = getIntent().getStringExtra("emailUser");

            initObjects();
        }
        catch (Exception e)
        {
           // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * This method is to initialize views
     */
    private void initViews() {
        textViewNameProfile = (TextView) findViewById(R.id.nameProfile);
        textViewEmailProfile = (TextView) findViewById(R.id.emailProfile);
        imageProfile = (ImageView) findViewById(R.id.imageAvatar);
        textViewCountryProfile = (TextView) findViewById(R.id.countryProfile);
        textViewLatitudeProfile = (TextView) findViewById(R.id.latitudeProfile);
        textViewLongitudeProfile = (TextView) findViewById(R.id.longitudeProfile);
        textViewAddressProfile = (TextView) findViewById(R.id.addressProfile);
        textViewCountrylocProfile = (TextView) findViewById(R.id.countrylocProfile);

    }

    /**
     * This method is to initialize objects to be used
     */
    private void initObjects() {
        try{

            databaseHelper = new DatabaseHelper(this);
            User userProfile= databaseHelper.UserByEmail(emailUser);
            textViewNameProfile.setText(userProfile.getName());
            textViewEmailProfile.setText(userProfile.getEmail());
            textViewCountryProfile.setText(userProfile.getCountry());
            textViewLatitudeProfile.setText(Double.toString(userProfile.getLatitude()));
            textViewLongitudeProfile.setText(Double.toString(userProfile.getLongitude()));
            textViewAddressProfile.setText(userProfile.getAddress());
            textViewCountrylocProfile.setText(userProfile.getCountryloc());


            Bitmap bitmap = BitmapFactory.decodeByteArray(userProfile.getImage(), 0, userProfile.getImage().length);
            imageProfile.setImageBitmap (bitmap);


        }catch (Exception e)
        {
         //   Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }







}
