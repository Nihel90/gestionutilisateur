package com.example.userauthentif;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 49;

    // Database Name
    private static final String DATABASE_NAME = "UserManager.db";

    // User table name
    private static final String TABLE_USER = "user";

    // User Table Columns names
    private static final String COLUMN_USER_ID = "user_id";
    private static final String COLUMN_USER_NAME = "user_name";
    private static final String COLUMN_USER_EMAIL = "user_email";
    private static final String COLUMN_USER_PASSWORD = "user_password";
    private static final String COLUMN_USER_COUNTRY = "user_country";
    private static final String COLUMN_USER_IMAGE = "user_image";
    private static final String COLUMN_USER_LATITUDE = "user_latitude";
    private static final String COLUMN_USER_LONGITUDE = "user_longitude";
    private static final String COLUMN_USER_ADDRESS = "user_address";
    private static final String COLUMN_USER_COUNTRYLOC = "user_countryloc";

    // create table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + COLUMN_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_USER_NAME + " TEXT,"
            + COLUMN_USER_EMAIL + " TEXT," + COLUMN_USER_PASSWORD + " TEXT,"
            + COLUMN_USER_COUNTRY + " TEXT,"
            + COLUMN_USER_IMAGE + " BLOB,"
            + COLUMN_USER_LATITUDE + " TEXT,"
            + COLUMN_USER_LONGITUDE + " TEXT,"
            + COLUMN_USER_ADDRESS + " TEXT,"
            + COLUMN_USER_COUNTRYLOC + " TEXT)";


    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;

    /**
     * Constructor
     *
     * @param context
     */



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(DROP_USER_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL("INSERT INTO  user   VALUES (1, 'ADMIN' , 'admin@admin.tn' , 'admin' , 'Tunisie' , null , '35.820176' , '10.5513455,17' , 'Rue de Grenades, 4021, Kalàa Sghira, Sousse' , 'Tunisie' )");
        db.execSQL("INSERT INTO  user   VALUES (2, 'Mohamed Ben Saleh' , 'mohamed@gmail.com' , 'mohamed' , 'Tunisie' , null , '35.8249901' , '10.6402155' , 'Rue Salah Saad, Sousse' , 'Tunisie' )");
       db.execSQL("INSERT INTO  user   VALUES (3, 'Youssef Ben Saad' , 'youssef@outlook.fr' , 'youssef' , 'Tunisie' , null , '36.4055958' , '10.6142014' , 'Avenue Habib Thameur, Hammamet' , 'Tunisie' )");
       db.execSQL("INSERT INTO  user   VALUES (4, 'Mohamed Ali Mokded' , 'dali@gmail.fr' , 'dali' , 'Tunisie' , null , '35.8196443' , '10.5915283' , 'Riadh, Sousse' , 'Tunisie' )");

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        //Drop User Table if exist
        db.execSQL(DROP_USER_TABLE);

        // Create tables again
        onCreate(db);

    }

    /**
     * This method is to create user record
     *
     * @param user
     */
    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COLUMN_USER_PASSWORD, user.getPassword());
        values.put(COLUMN_USER_COUNTRY, user.getCountry());
        values.put(COLUMN_USER_IMAGE, user.getImage());
        values.put(COLUMN_USER_LATITUDE, user.getLatitude());
        values.put(COLUMN_USER_LONGITUDE, user.getLongitude());
        values.put(COLUMN_USER_ADDRESS, user.getAddress());
        values.put(COLUMN_USER_COUNTRYLOC, user.getCountryloc());

        // Inserting Row
        db.insert(TABLE_USER, null, values);
        db.close();
    }

    /**
     * This method is to fetch all user and return the list of user records
     *
     * @return list
     */
    public List<User> getAllUser() {
        // array of columns to fetch

        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_EMAIL,
                COLUMN_USER_NAME,
                COLUMN_USER_PASSWORD,
                COLUMN_USER_COUNTRY,
                COLUMN_USER_IMAGE,
                COLUMN_USER_LATITUDE,
                COLUMN_USER_LONGITUDE,
                COLUMN_USER_ADDRESS,
                COLUMN_USER_COUNTRYLOC
        };


        // sorting orders
        String sortOrder =
                COLUMN_USER_ID + " DESC";
        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,    //columns to return
                null,        //columns for the WHERE clause
                null,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                sortOrder); //The sort order

        // Traversing through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                User user = new User();
                user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID))));
                user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
                user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_USER_PASSWORD)));
                user.setCountry(cursor.getString(cursor.getColumnIndex(COLUMN_USER_COUNTRY)));
                user.setImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_USER_IMAGE)));
                user.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_USER_LATITUDE)));
                user.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_USER_LONGITUDE)));
                user.setAddress(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ADDRESS)));
                user.setCountryloc(cursor.getString(cursor.getColumnIndex(COLUMN_USER_COUNTRYLOC)));

                // Adding user record to list
                userList.add(user);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        // return user list
        return userList;
    }

    public User UserByEmail(String email) {
        // array of columns to fetch

        String[] columns = {
                COLUMN_USER_ID,
                COLUMN_USER_EMAIL,
                COLUMN_USER_NAME,
                COLUMN_USER_PASSWORD,
                COLUMN_USER_COUNTRY,
                COLUMN_USER_IMAGE,
                COLUMN_USER_LATITUDE,
                COLUMN_USER_LONGITUDE,
                COLUMN_USER_ADDRESS,
                COLUMN_USER_COUNTRYLOC
        };

        String whereClause = COLUMN_USER_EMAIL +"= ?";
        String[] whereArgs = new String[] {
                email,
        };
        // sorting orders

        List<User> userList = new ArrayList<User>();

        SQLiteDatabase db = this.getReadableDatabase();

        // query the user table
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id,user_name,user_email,user_password FROM user ORDER BY user_name;
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,    //columns to return
                whereClause,        //columns for the WHERE clause
                whereArgs,        //The values for the WHERE clause
                null,       //group the rows
                null,       //filter by row groups
                null); //The sort order

        // Traversing through all rows and adding to list
        User user = new User();
        if (cursor.moveToFirst()) {

            user.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID))));
            user.setName(cursor.getString(cursor.getColumnIndex(COLUMN_USER_NAME)));
            user.setEmail(cursor.getString(cursor.getColumnIndex(COLUMN_USER_EMAIL)));
            user.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_USER_PASSWORD)));
            user.setCountry(cursor.getString(cursor.getColumnIndex(COLUMN_USER_COUNTRY)));
            user.setImage(cursor.getBlob(cursor.getColumnIndex(COLUMN_USER_IMAGE)));
            user.setLatitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_USER_LATITUDE)));
            user.setLongitude(cursor.getDouble(cursor.getColumnIndex(COLUMN_USER_LONGITUDE)));
            user.setAddress(cursor.getString(cursor.getColumnIndex(COLUMN_USER_ADDRESS)));
            user.setCountryloc(cursor.getString(cursor.getColumnIndex(COLUMN_USER_COUNTRYLOC)));
            // Adding user record to list
        }

        cursor.close();
        db.close();

        // return user list
        return user;
    }
    /**
     * This method to update user record
     *
     * @param user
     */
    public void updateUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, user.getName());
        values.put(COLUMN_USER_EMAIL, user.getEmail());
        values.put(COLUMN_USER_PASSWORD, user.getPassword());
        values.put(COLUMN_USER_COUNTRY, user.getCountry());
        values.put(COLUMN_USER_IMAGE, user.getImage());
        values.put(COLUMN_USER_LATITUDE, user.getLatitude());
        values.put(COLUMN_USER_LONGITUDE, user.getLongitude());
        values.put(COLUMN_USER_ADDRESS, user.getAddress());
        values.put(COLUMN_USER_COUNTRYLOC, user.getCountryloc());

        // updating row
        db.update(TABLE_USER, values, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method is to delete user record
     *
     * @param user
     */
    public void deleteUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        // delete user record by id
        db.delete(TABLE_USER, COLUMN_USER_ID + " = ?",
                new String[]{String.valueOf(user.getId())});
        db.close();
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @return true/false
     */
    public boolean checkUser(String email) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();

        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?";

        // selection argument
        String[] selectionArgs = {email};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();

        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

    /**
     * This method to check user exist or not
     *
     * @param email
     * @param password
     * @return true/false
     */
    public boolean checkUser(String email, String password) {

        // array of columns to fetch
        String[] columns = {
                COLUMN_USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COLUMN_USER_EMAIL + " = ?" + " AND " + COLUMN_USER_PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_USER, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }
}